﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sinu.Models
{
    public class Result
    {
        public string id;
        public string[] data;
        public Result(string id, string[] data)
        {
            this.id = id;
            this.data = data;
        }
    }
}