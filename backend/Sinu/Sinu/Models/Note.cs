﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sinu.Models
{
    public class Note
    {
        public string idNota;
        public string idMaterie;
        public string idStudent;
        public string nota;

        public string IdNota { get => idNota; set => idNota = value; }
        public string IdMaterie { get => idMaterie; set => idMaterie = value; }
        public string IdStudent { get => idStudent; set => idStudent = value; }
        public string Nota { get => nota; set => nota = value; }
    }
}