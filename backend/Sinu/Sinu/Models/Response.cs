﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sinu.Models
{
    public class Response
    {
        public List<Result> rows;
        public Response(List<Result> rows)
        {
            this.rows = rows;
        }
    }
}