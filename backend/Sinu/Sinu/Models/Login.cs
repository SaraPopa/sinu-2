﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sinu.Models
{
    public class Login
    {
        private string username;
        private string password;

        public string Username { get => username; set => username = value; }
        public string Password { get => password; set => password = value; }
    }
}