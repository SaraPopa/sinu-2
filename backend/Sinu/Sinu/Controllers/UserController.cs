﻿using Sinu.Models;
using Sinu.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Sinu.Controllers
{
    public class UserController : ApiController
    {
        UserRepo usr = new UserRepo();
        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/updateStudentBySecretar")]
        public IHttpActionResult updatePerson([FromBody]User user)
        {
            var test = usr.UpdatePersonBySecretar(user) ;
            return Ok(test);
        }
    }
}
