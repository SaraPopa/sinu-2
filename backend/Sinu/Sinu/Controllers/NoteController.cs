﻿using Sinu.Models;
using Sinu.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Sinu.Controllers
{
    public class NoteController : ApiController
    {
        NoteRepo n = new NoteRepo();
        [HttpGet]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/getNote")]
        public IHttpActionResult getNoteByIdMaterie(int id)
        {
            return Ok(n.GetNote(id));
        }

    }
}
