﻿using Sinu.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Sinu.Controllers
{
    public class TstController : ApiController
    {
        Test t = new Test();
        [HttpGet]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/gettest")]
        public IHttpActionResult getTest(string id)
        {
            return Ok(t.getDataTest(id));
        }
    }
}
