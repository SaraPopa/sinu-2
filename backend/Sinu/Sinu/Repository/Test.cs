﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Sinu.Repository
{
    public class Test
    {
        DBConnection db = new DBConnection();
        public string getDataTest(string id)
        {
            string result = "";
            SqlConnection con = db.GetConnection();
            try
            {
                string query = @"select * from dbo.test where tst=@id";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@id", id);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {

                    result = reader["tst"].ToString();

                }
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
    }

        
}