﻿using Sinu.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Sinu.Repository
{
    public class NoteRepo
    {
        DBConnection db = new DBConnection();
        public Response GetNote(int id)
        {
            SqlConnection con = db.GetConnection();
            try
            {
                string query = @"select * from [BDSinu].[dbo].[Nota] where IdMaterie=@id";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@id", id);
                SqlDataReader reader = cmd.ExecuteReader();
                List<Result> resultList = new List<Result>();
                while (reader.Read())
                {
                    List<string> data = new List<string>();
                    data.Add(reader["idNota"].ToString());
                    data.Add(reader["idMaterie"].ToString());
                    data.Add(reader["IdStudent"].ToString());
                    data.Add(reader["nota"].ToString());
                    
                   
                    resultList.Add(new Result(reader[0].ToString(), data.ToArray()));

                }
                Response r = new Response(resultList);
                return r;



            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }
    }
}