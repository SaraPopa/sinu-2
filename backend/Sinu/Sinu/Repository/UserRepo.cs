﻿using Sinu.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Sinu.Repository
{
    public class UserRepo
    {
        DBConnection db = new DBConnection();
        public string UpdatePersonBySecretar(User user)
        {
            SqlConnection con = db.GetConnection();
            try
            {
                if (user.IdUser!= 0)
                {
                    string query = @"UPDATE [BDSinu].[dbo].[User] SET nume =@nume , prenume=@prenume,telefon=@telefon,CNP=@cnp,email=@email,imagine=@imagine WHERE idUser=@idUser";
                   
                        SqlCommand cmd = new SqlCommand(query, con);

                    cmd.Parameters.AddWithValue("@idUser", user.IdUser);
                    cmd.Parameters.AddWithValue("@imagine", user.Imagine);
                    cmd.Parameters.AddWithValue("@email", user.Email);
                    cmd.Parameters.AddWithValue("@cnp", user.Cnp);
                   cmd.Parameters.AddWithValue("@prenume", user.Prenume);
                    cmd.Parameters.AddWithValue("@nume", user.Nume);
                    cmd.Parameters.AddWithValue("@telefon", user.Telefon);
                    cmd.ExecuteNonQuery();


                }

                return "ok";

            }
            catch (Exception e) { throw e; }
            finally
            {
                con.Close();
                con.Dispose();
            }


        }
    }
}