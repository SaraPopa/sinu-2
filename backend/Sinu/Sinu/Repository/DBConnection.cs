﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Sinu.Repository
{
    public class DBConnection
    {
        public SqlConnection GetConnection()
        {
            try
            {
                SqlConnection myConnection = new SqlConnection("Server=DESKTOP-0CEDNKP\\SQLEXPRESS;" + "Database=BDSinu;" + "Connection timeout=30;" + "Integrated Security = SSPI;");
                myConnection.Open();
                return myConnection;

            }
            catch (Exception ex) { throw ex; }
        }
    }
}