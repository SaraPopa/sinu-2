﻿using Sinu.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Sinu.Repository
{
    public class LoginRepo
    {
        public string getRol(string username, string password)
        {
            string rol = "";
            DBConnection db = new DBConnection();
            SqlConnection con = db.GetConnection();
            try
            {
                string query = @"select[rol] from[BDSinu].[dbo].[User] where((username = @username or email = @email) and password = @password)";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@username", username);
                cmd.Parameters.AddWithValue("@email", username);
                cmd.Parameters.AddWithValue("@password", password);
                SqlDataReader reader = cmd.ExecuteReader();
               
                if (reader.HasRows)
                while (reader.Read())
                {
                   rol=reader["rol"].ToString();

                }
                
                return rol;



            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
    }
}