USE [master]
GO
/****** Object:  Database [BDSinu]    Script Date: 10-Apr-19 18:16:14 ******/
CREATE DATABASE [BDSinu]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'BDSinu', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\BDSinu.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'BDSinu_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\BDSinu_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [BDSinu] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BDSinu].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BDSinu] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BDSinu] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BDSinu] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BDSinu] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BDSinu] SET ARITHABORT OFF 
GO
ALTER DATABASE [BDSinu] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [BDSinu] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [BDSinu] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BDSinu] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BDSinu] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [BDSinu] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BDSinu] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BDSinu] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BDSinu] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BDSinu] SET  DISABLE_BROKER 
GO
ALTER DATABASE [BDSinu] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BDSinu] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [BDSinu] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [BDSinu] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [BDSinu] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BDSinu] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [BDSinu] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [BDSinu] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [BDSinu] SET  MULTI_USER 
GO
ALTER DATABASE [BDSinu] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [BDSinu] SET DB_CHAINING OFF 
GO
ALTER DATABASE [BDSinu] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [BDSinu] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [BDSinu] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [BDSinu] SET QUERY_STORE = OFF
GO
USE [BDSinu]
GO
/****** Object:  Table [dbo].[Facultate]    Script Date: 10-Apr-19 18:16:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Facultate](
	[idFacultate] [int] IDENTITY(1,1) NOT NULL,
	[denumire] [varchar](50) NULL,
	[specializare] [varchar](50) NULL,
 CONSTRAINT [PK_Facultate] PRIMARY KEY CLUSTERED 
(
	[idFacultate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Grupa]    Script Date: 10-Apr-19 18:16:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Grupa](
	[idGrupa] [int] IDENTITY(1,1) NOT NULL,
	[numarGrupa] [int] NULL,
	[an] [varchar](50) NULL,
	[nrMaxStudentiPerGrupa] [int] NULL,
 CONSTRAINT [PK_Grupa] PRIMARY KEY CLUSTERED 
(
	[idGrupa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Materie]    Script Date: 10-Apr-19 18:16:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Materie](
	[idMaterie] [int] IDENTITY(1,1) NOT NULL,
	[denumire] [varchar](50) NULL,
	[idProfesor] [int] NULL,
	[numarDeCredite] [int] NULL,
 CONSTRAINT [PK_Materie] PRIMARY KEY CLUSTERED 
(
	[idMaterie] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Nota]    Script Date: 10-Apr-19 18:16:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Nota](
	[idNota] [int] IDENTITY(1,1) NOT NULL,
	[idMaterie] [int] NULL,
	[idStudent] [int] NULL,
	[nota] [int] NULL,
 CONSTRAINT [PK_Nota] PRIMARY KEY CLUSTERED 
(
	[idNota] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sesiune]    Script Date: 10-Apr-19 18:16:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sesiune](
	[idSesiune] [int] IDENTITY(1,1) NOT NULL,
	[idMaterie] [int] NOT NULL,
	[dataSiOra] [datetime] NULL,
	[sala] [varchar](50) NULL,
 CONSTRAINT [PK_Sesiune] PRIMARY KEY CLUSTERED 
(
	[idSesiune] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SpecializareMaterie]    Script Date: 10-Apr-19 18:16:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SpecializareMaterie](
	[idSpecializareMaterie] [int] IDENTITY(1,1) NOT NULL,
	[idFacultate] [int] NULL,
	[idMaterie] [int] NULL,
 CONSTRAINT [PK_SpecializareMaterie] PRIMARY KEY CLUSTERED 
(
	[idSpecializareMaterie] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StudentGrupa]    Script Date: 10-Apr-19 18:16:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudentGrupa](
	[idStudentGrupa] [int] IDENTITY(1,1) NOT NULL,
	[idStudent] [int] NULL,
	[idGrupa] [int] NULL,
 CONSTRAINT [PK_StudentGrupa] PRIMARY KEY CLUSTERED 
(
	[idStudentGrupa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StudentMaterie]    Script Date: 10-Apr-19 18:16:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudentMaterie](
	[idStudentMaterie] [int] IDENTITY(1,1) NOT NULL,
	[idStudent] [int] NULL,
	[idMaterie] [int] NULL,
 CONSTRAINT [PK_StudentMaterie] PRIMARY KEY CLUSTERED 
(
	[idStudentMaterie] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 10-Apr-19 18:16:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[idUser] [int] IDENTITY(1,1) NOT NULL,
	[username] [varchar](50) NULL,
	[password] [varchar](50) NULL,
	[nume] [varchar](50) NULL,
	[prenume] [varbinary](50) NULL,
	[telefon] [varchar](50) NULL,
	[CNP] [varchar](50) NULL,
	[rol] [varchar](50) NULL,
	[email] [varchar](50) NULL,
	[imagine] [varchar](max) NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[idUser] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Nota]  WITH CHECK ADD  CONSTRAINT [FK_Nota_Materie] FOREIGN KEY([idMaterie])
REFERENCES [dbo].[Materie] ([idMaterie])
GO
ALTER TABLE [dbo].[Nota] CHECK CONSTRAINT [FK_Nota_Materie]
GO
ALTER TABLE [dbo].[Nota]  WITH CHECK ADD  CONSTRAINT [FK_Nota_User] FOREIGN KEY([idStudent])
REFERENCES [dbo].[User] ([idUser])
GO
ALTER TABLE [dbo].[Nota] CHECK CONSTRAINT [FK_Nota_User]
GO
ALTER TABLE [dbo].[Sesiune]  WITH CHECK ADD  CONSTRAINT [FK_Sesiune_Materie] FOREIGN KEY([idMaterie])
REFERENCES [dbo].[Materie] ([idMaterie])
GO
ALTER TABLE [dbo].[Sesiune] CHECK CONSTRAINT [FK_Sesiune_Materie]
GO
ALTER TABLE [dbo].[SpecializareMaterie]  WITH CHECK ADD  CONSTRAINT [FK_SpecializareMaterie_Facultate] FOREIGN KEY([idFacultate])
REFERENCES [dbo].[Facultate] ([idFacultate])
GO
ALTER TABLE [dbo].[SpecializareMaterie] CHECK CONSTRAINT [FK_SpecializareMaterie_Facultate]
GO
ALTER TABLE [dbo].[SpecializareMaterie]  WITH CHECK ADD  CONSTRAINT [FK_SpecializareMaterie_Materie] FOREIGN KEY([idMaterie])
REFERENCES [dbo].[Materie] ([idMaterie])
GO
ALTER TABLE [dbo].[SpecializareMaterie] CHECK CONSTRAINT [FK_SpecializareMaterie_Materie]
GO
ALTER TABLE [dbo].[StudentGrupa]  WITH CHECK ADD  CONSTRAINT [FK_StudentGrupa_Grupa] FOREIGN KEY([idGrupa])
REFERENCES [dbo].[Grupa] ([idGrupa])
GO
ALTER TABLE [dbo].[StudentGrupa] CHECK CONSTRAINT [FK_StudentGrupa_Grupa]
GO
ALTER TABLE [dbo].[StudentGrupa]  WITH CHECK ADD  CONSTRAINT [FK_StudentGrupa_User] FOREIGN KEY([idStudent])
REFERENCES [dbo].[User] ([idUser])
GO
ALTER TABLE [dbo].[StudentGrupa] CHECK CONSTRAINT [FK_StudentGrupa_User]
GO
ALTER TABLE [dbo].[StudentMaterie]  WITH CHECK ADD  CONSTRAINT [FK_StudentMaterie_Materie] FOREIGN KEY([idMaterie])
REFERENCES [dbo].[Materie] ([idMaterie])
GO
ALTER TABLE [dbo].[StudentMaterie] CHECK CONSTRAINT [FK_StudentMaterie_Materie]
GO
ALTER TABLE [dbo].[StudentMaterie]  WITH CHECK ADD  CONSTRAINT [FK_StudentMaterie_User] FOREIGN KEY([idStudent])
REFERENCES [dbo].[User] ([idUser])
GO
ALTER TABLE [dbo].[StudentMaterie] CHECK CONSTRAINT [FK_StudentMaterie_User]
GO
USE [master]
GO
ALTER DATABASE [BDSinu] SET  READ_WRITE 
GO
