import React from 'react';
import './Toolbar.css';
import logoImage from '../Images/SinuLogo.jpg';

const toolbar = props => (
    <div> 
        <span className = "toolbar">
             <button className ="toolbar_button"> Note</button>             
        </span>
        <span className = "logo">
            <img id="image" src={logoImage}></img>
        </span>
        
    </div>
);
export default toolbar;